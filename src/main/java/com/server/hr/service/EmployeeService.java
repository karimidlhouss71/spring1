package com.server.hr.service;


import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.server.hr.exceptions.UserNotFoundException;
import com.server.hr.model.Cv;
import com.server.hr.model.Employee;
import com.server.hr.model.File;
import com.server.hr.model.Image;
import com.server.hr.model.Leave;
import com.server.hr.reposi.CvRepo;
import com.server.hr.reposi.EmployeeRepo;
import com.server.hr.reposi.ImageRepo;
import com.server.hr.reposi.LeaveTypeRepo;



@Service
@Transactional
public class EmployeeService {
	private final EmployeeRepo employeeRepo;
	private final LeaveTypeRepo leaveTypeRepo;
	private final FileService fileService;
	private final CvRepo cvRepo;
	private final ImageRepo imageRepo;

	@Autowired
	public EmployeeService(EmployeeRepo employeeRepo, LeaveTypeRepo leaveTypeRepo, CvRepo cvRepo, ImageRepo imageRepo, FileService fileService) {
		this.employeeRepo = employeeRepo;
		this.leaveTypeRepo = leaveTypeRepo;
		this.fileService = fileService;
		this.cvRepo = cvRepo;
		this.imageRepo = imageRepo;
	}
	public Employee addEmployee(Employee employee,MultipartFile cv,MultipartFile image) {
		File cvFile = fileService.save(cv);
		File imageFile = fileService.save(image);
		Cv newCv = new Cv();
		newCv.setNom(cvFile.getName());
		newCv.setUrl(cvFile.getUrl());
		Image newImage = new Image();
		newImage.setNom(imageFile.getName());
		newImage.setUrl(imageFile.getUrl());
		newCv = cvRepo.save(newCv);
		newImage = imageRepo.save(newImage);
		employee.setCv(newCv);
		employee.setImage(newImage);
		employee = employeeRepo.save(employee);
		newCv.setEmployee(employee);
		newImage.setEmployee(employee);
		cvRepo.save(newCv);
		imageRepo.save(newImage);
		return employee;
	}
	public List<Employee> findAllEmployee(){
		return employeeRepo.findAll();
	}
	public Employee updateEmployee(Employee employee) {
		//employee.setCv(cvRepo.findByEmployeeId(employee.getId()).get());
		//employee.setImage(imageRepo.findByEmployeeById(employee.getId()).get());
		return employeeRepo.save(employee);
	}
	public List<Employee> getEmployeesByIsArchived(Boolean isArchived) {
		return employeeRepo.getEmployeeByIsArchived(isArchived);
	}
	public void changeEmployeeStatus(Long id) {
		Employee employee = this.findEmployeeById(id);
		employee.setIsArchived(!employee.getIsArchived());
	}
	public Employee findEmployeeById(Long id) {
		
		Optional<Employee> employee = employeeRepo.findEmployeeById(id);
		
		Employee storedEmployee = employee.get();
		
		return this.leaveBalanceCalcul(storedEmployee);
	}
	public void deleteEmployee(Long id) {
		employeeRepo.deleteEmployeeById(id);
	}
	
	public Employee leaveBalanceCalcul(Employee employee) {
		LocalDate endDate = LocalDate.now().withMonth(12).withDayOfMonth(31);	
		LocalDate startDate = endDate.minusYears(1).withMonth(1).withDayOfMonth(1);
		
		Date endD = Date.from(endDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date startD = Date.from(startDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		
		List<Leave> leaves = employee.getLeaves().stream()
				.filter(leave -> leave.getLeaveType().equals(leaveTypeRepo.findLeaveTypeByNom("Administrative").get()))
						.filter(leave -> leave.getDate_debut().after(startD) 
								&& leave.getDate_debut().before(endD) ).collect(Collectors.toList());

		int count = leaves.stream().map(leave -> leave.getDureeConge() - leave.getRestDays()).mapToInt(i-> i.intValue()).sum();
		
		employee.setLeaveBalance(44 - count);
		
		return employee;
	}
	

}
