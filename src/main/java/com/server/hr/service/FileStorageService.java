package com.server.hr.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.server.hr.model.ResponseBean;

public interface FileStorageService {

	String storeFile(MultipartFile file);

	String storeLogo(MultipartFile file);

	Resource loadFileAsResource(String fileName);

	ResponseBean<Boolean> deleteFile(String fileName);

}
