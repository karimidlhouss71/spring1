package com.server.hr.reposi;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.server.hr.model.Cv;

public interface CvRepo extends JpaRepository<Cv, Long>{
	
	//Optional<Cv> findByEmployeeId(Long id);

}
