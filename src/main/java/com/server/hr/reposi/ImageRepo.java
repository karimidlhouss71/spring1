package com.server.hr.reposi;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.server.hr.model.Image;

public interface ImageRepo extends JpaRepository<Image, Long>{
   
	/*Optional<Image> findByEmployeeById(Long id);*/
}
