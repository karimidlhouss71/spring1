package com.server.hr.exceptions;

public class FileStorageException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 9188540976259536525L;

    public FileStorageException(String message) {
        super(message);
    }

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
