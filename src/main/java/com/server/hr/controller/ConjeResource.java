package com.server.hr.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import com.server.hr.model.Leave;

import com.server.hr.service.ConjeService;
import com.server.hr.service.FileStorageService;

@RestController
@RequestMapping("/conje")
public class ConjeResource {
	private final ConjeService conjeService;

	@Autowired
	private FileStorageService fileStorageService;

	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
	private static Font smallFont = new Font(Font.FontFamily.HELVETICA, 12);

	private static BaseColor colorBlue = new BaseColor(43, 170, 215);
	private static Font titleFontProfil = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD, colorBlue);
	private static Font smallTitleFontProfil = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, colorBlue);
	private static Font smallestTitleFontProfil = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, colorBlue);
	private static Font titleFontProfilBlack = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);
	private static Font smallTitleFontProfilBlack = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	private static Font smallestTitleFontProfilBlack = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);

	public ConjeResource(ConjeService conjeService) {
		this.conjeService = conjeService;
	}

	@GetMapping("/all")
	public ResponseEntity<List<Leave>> getAllConje() {
		List<Leave> conje = conjeService.findAllConje();
		return new ResponseEntity<>(conje, HttpStatus.OK);
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Leave> getConjeById(@PathVariable("id") Long id) {
		Leave conje = conjeService.findConjeById(id);
		return new ResponseEntity<>(conje, HttpStatus.OK);
	}

	@GetMapping("/employee/{id}")
	public ResponseEntity<List<Leave>> getCongeEmployeeById(@PathVariable("id") Long id) {
		List<Leave> conge = conjeService.findcongeEmployee(id);
		return new ResponseEntity<>(conge, HttpStatus.OK);
	}

	@PostMapping("/add")
	public ResponseEntity<Leave> addConje(@RequestBody Leave conje) {
		Leave newconje = conjeService.addConje(conje);
		return new ResponseEntity<>(newconje, HttpStatus.CREATED);
	}

	@PutMapping("/update")
	public ResponseEntity<Leave> updateConje(@RequestBody Leave conje) {
		Leave updateconje = conjeService.UpdateConje(conje);
		return new ResponseEntity<>(updateconje, HttpStatus.OK);
	}

	@DeleteMapping("/delete/{id_c}")
	public void deleteConje(@PathVariable("id_c") Long id_c) {
		conjeService.deleteConjeById(id_c);
		//return new ResponseEntity<String>("Conje is deleted successfully !!!", HttpStatus.OK);
	}

	@PostMapping("/downloadLeave/{id}")
	public ResponseEntity<Resource> downloadLeave(@PathVariable Long id, HttpServletRequest request)
			throws IOException, DocumentException {

		Leave conje = conjeService.findConjeById(id);

		Document document = new Document();
		// document.setPageSize(PageSize.A4.rotate());
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("uploads/leave.pdf"));
		document.open();

		PdfPTable table = new PdfPTable(1);
		PdfPCell c;

		c = new PdfPCell(new Phrase("Congé", titleFontProfil));
		c.setHorizontalAlignment(Element.ALIGN_CENTER);
		c.setBorder(0);
		table.addCell(c);

		Paragraph preface = new Paragraph("ثثس");
		document.add(table);
		document.add(preface);
		document.add(new Paragraph("  "));
		// ////////////////////////////////////////////////////////////////////////
		//
		table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setWidths(new int[] { 2, 3, 2, 3 });

		//// Line 1

		c = new PdfPCell(new Phrase("Employé:", smallestTitleFontProfilBlack));
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		c = new PdfPCell(new Phrase(conje.getEmployee().getNom() + " " + conje.getEmployee().getPrenom()));
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		c = new PdfPCell(new Phrase("Type: ", smallestTitleFontProfilBlack));
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		c = new PdfPCell(new Phrase(conje.getLeaveType().getNom()));
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		//// Line 2

		c = new PdfPCell(new Phrase("Date Début: ", smallestTitleFontProfilBlack));
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		c = new PdfPCell(new Phrase(conje.getDate_debut().toString()));
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		c = new PdfPCell(new Phrase("Date Fin: ", smallestTitleFontProfilBlack));
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		c = new PdfPCell(new Phrase(conje.getDate_fin().toString()));
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		//// Line 3

		c = new PdfPCell(new Phrase("Durée Congé:", smallestTitleFontProfilBlack));
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		if (conje.getDureeConge() == null) {
			c = new PdfPCell(new Phrase(""));
		} else {
			c = new PdfPCell(new Phrase(conje.getDureeConge()));
		}
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		c = new PdfPCell(new Phrase("Département:", smallestTitleFontProfilBlack));
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		c = new PdfPCell(new Phrase(" "));
		c.setHorizontalAlignment(Element.ALIGN_LEFT);
		c.setBorder(0);
		table.addCell(c);

		document.add(table);
		document.add(new Paragraph("  "));

		Paragraph head = new Paragraph("___________________________________________________________________");
		document.add(head);

		// ///////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////

		document.close();

		// Load file as Resource
		Resource resource = fileStorageService.loadFileAsResource("leave.pdf");

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			// logger.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

}
